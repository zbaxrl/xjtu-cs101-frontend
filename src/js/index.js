import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import '@mdi/font/css/materialdesignicons.css'
import VueRouter from "vue-router"
import App from './App.vue'

Vue.use(VueRouter)
Vue.use(Buefy)

new Vue({ render: createElement => createElement(App) }).$mount('#app')
